object MainSegundaParte {
  def main(args: Array[String]): Unit = {
    
    var lista = new NoConta();
    
    lista = lista.criarLista()
    
    var contaBancaria = new ContaBancaria(null,0.0);
    var contaPoupanca = new ContaPoupanca(null,0.0);
    var contaFidelidade = new ContaFidelidade(null,0.0);
    
    contaBancaria.numeroConta = 3
    contaBancaria.saldo = 100.00
    
    contaPoupanca.numeroConta = 2
    contaPoupanca.saldo = 400.00
    
    contaFidelidade.numeroConta = 5
    contaFidelidade.saldo = 243.00
    
    //primeira
    lista = lista.inserir(lista, contaBancaria)
    //segunda
    lista = lista.inserir(lista, contaPoupanca)
    //terceira
    lista = lista.inserir(lista, contaFidelidade)
    
    //quarta
    contaBancaria.creditar(250.00)
    
    
    //quinta
    contaFidelidade.debitar(100.00)
    
    //sexta
    println("****************** Sexta ******************")
    println("Novo saldo conta fidelidade: " + contaFidelidade.saldo)
    println("Novo saldo conta bancaria: " + contaBancaria.saldo)
    
    //setima
    println("****************** Sétima ******************")
    println("Bonus conta fidelidade: " + contaFidelidade.bonus)
    
    //oitava
    println("****************** Oitava ******************")
    
    //nona
    println("****************** Nona ******************")
    contaPoupanca.renderJuros(contaPoupanca.saldo, 0.5)
    
    //Décima
    println("****************** Décima ******************")
    contaFidelidade.rederBonus(contaFidelidade.saldo)
    
    //Decima segunda
    println("****************** Décima segunda******************")
    lista.imprimir(lista)
    
    //Décima primeira
    println("****************** Décima Primeira ******************")
    lista.remover(lista, 5)
    lista.imprimir(lista)
    
  }
}