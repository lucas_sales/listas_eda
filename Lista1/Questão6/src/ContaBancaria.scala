class ContaBancaria(numeroConta:Integer, valor:Double) extends Conta {
  
  override def creditar(valor: Double): Unit ={
    this.saldo = this.saldo + valor
  }
  
  override def debitar(valor: Double): Unit ={
    this.saldo = this.saldo - valor
  }
  
}