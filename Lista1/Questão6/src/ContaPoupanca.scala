class ContaPoupanca(numeroConta:Integer, valor:Double) extends Conta{
  
  var bonus:Double = 0;
  
  override def creditar(valor: Double): Unit ={
    this.saldo = this.saldo + valor
  }
  
  override def debitar(valor: Double): Unit ={
    this.saldo = this.saldo - valor
  }
  
  def renderJuros(saldo:Double, juros:Double ): Unit ={
    var valorJuros = saldo*juros
    this.saldo = this.saldo + valorJuros
  }
}