object Main {
  def main(args: Array[String]): Unit = {
    var lista = new NoConta();
    
    //Primeira
    lista = lista.criarLista()
    
    var contaBancaria = new ContaBancaria(null,0.0);
    var contaPoupanca = new ContaPoupanca(null,0.0);
    var contaFidelidade = new ContaFidelidade(null,0.0);
    
    contaBancaria.numeroConta = 3
    contaBancaria.saldo = 100.00
    
    contaPoupanca.numeroConta = 2
    contaPoupanca.saldo = 400.00
    
    contaFidelidade.numeroConta = 5
    contaFidelidade.saldo = 243.00
    
    //Segunda 
    lista = lista.inserir(lista, contaBancaria)
    lista = lista.inserir(lista, contaPoupanca)
    lista = lista.inserir(lista, contaFidelidade)
    
    //Terceira
    println("******************imprimir contas******************")
    lista.imprimir(lista)
    
    //Quarta
    println("******************imprimir contas recursivo******************")
    lista.imprimirRecursivo(lista)
    
    //Quinta
    println("******************imprimir a contas reverso******************")
    lista.imprimirReverso(lista)
    
    //Sexta
    println("*****************Lista vazia******************")
    println("Vazia? " + lista.isEmpty(lista))
    
    //Setima
    println("******************Buscar conta '2' e conta '5' ******************")
    println("Numero conta Elemento 1: " + lista.buscarConta(lista, 2).conta.numeroConta)
    println("Numero conta Elemento 2: " + lista.buscarConta(lista, 2).conta.saldo)
    println("Numero conta Elemento 1: " + lista.buscarConta(lista, 5).conta.numeroConta)
    println("Numero conta Elemento 2: " + lista.buscarConta(lista, 5).conta.saldo)
    
    //Oitava
    println("******************remover conta '3' da lista******************")
    lista = lista.remover(lista, 3)
    lista.imprimir(lista)
    
    //Nona
    println("******************remover conta '2' lista recursivo******************")
    lista = lista.removerRecursivo(lista, 2)
    lista.imprimir(lista)
    
    //Decima
    println("******************Liberar Lista******************")
    //lista = lista.liberar(lista)
    //lista.imprimir(lista)

    
  }  
}