class Conta extends TConta{
  var numeroConta:Integer = _;
  var saldo:Double = 0;
  
  override def creditar(valor: Double): Unit ={
    this.saldo = this.saldo + valor
  }
  
  override def debitar(valor: Double): Unit ={
    this.saldo = this.saldo - valor
  }

}