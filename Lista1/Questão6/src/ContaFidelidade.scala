class ContaFidelidade(numeroConta:Integer, valor:Double) extends Conta{
  var bonus:Double = _;
  
  override def creditar(valor: Double): Unit ={
    var b = 0.01*valor
    
    this.bonus = this.bonus + b
    this.saldo = this.saldo + valor
  }
  
  override def debitar(valor: Double): Unit ={
    this.saldo = this.saldo - valor
  }
  
  
  def rederBonus(valor:Double): Unit ={
    this.saldo = this.saldo + this.bonus
    this.bonus = 0;
  }
}