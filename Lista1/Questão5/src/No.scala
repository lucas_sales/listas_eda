class No(ant: No,  prox: No, valor: Integer) extends TNo{
  
  var anteriorNo : No = ant
  var proximoNo : No = prox
  var valorNo: Integer = valor
  
  override def criarLista(): No = {
    var lista = new No(null,null,null)
    lista
  }
  override def inserirLista(listaNo: No, valor: Integer): No = {
    var lista = listaNo
    if(lista.valorNo != null){
      var aux = listaNo.proximoNo
      while (aux.proximoNo != listaNo){
        aux = aux.proximoNo  
      }
      var novaLista = new No(null, null,valor)
      aux.proximoNo = novaLista
      novaLista.proximoNo = lista
      novaLista.anteriorNo = aux
      return lista
    }else{
       var novaLista = new No(null, null,valor)
       novaLista.proximoNo = novaLista
       novaLista.anteriorNo = novaLista
      return novaLista
    }
    
  }
  override def imprimirLista(lista: No): Unit ={
   var listaProx = lista
   
   if(listaProx != null){
     do{
    	 println("Valor No:" + listaProx.valorNo)
    	 listaProx = listaProx.proximoNo
       
     }while(listaProx != lista)
       
   }else{
     println("Lista está vazia")
   }
  }
  
  override def imprimirListaRecursiva(lista: No): Unit = {
    if(lista.anteriorNo != null){
      // println("valor é: " + lista.proximoNo.valorNo) Certo
      println("valor é: " + lista.valorNo)
      imprimirListaRecursiva(lista.anteriorNo)
    }
  }

  override def isEmpty(lista: No): Int = {
    if(lista.valorNo == null)
      return 1
    else
      return 0
  }
  override def buscarElemento(lista: No, valor: Integer): No = {
    var noAux = lista.proximoNo
    while(noAux.proximoNo != noAux){
      if(noAux.valorNo == valor)
        return noAux
      noAux = noAux.proximoNo
    }
    return null
  }
  override def removerElemento(lista: No, valor: Integer): No ={
    var novaLista  = lista
    var aux = lista.proximoNo
    var ant:No = null 
    if(buscarElemento(lista, valor) != null){
      
      while(aux != lista && aux.valorNo != valor){
        ant = aux
        aux = aux.proximoNo
      }
      
      if(ant == null){
        novaLista = novaLista.proximoNo
      }else if(aux == novaLista){
        novaLista = novaLista.proximoNo
        ant.proximoNo = novaLista
      }else{
        ant.proximoNo = aux.proximoNo
        aux = ant
      }
      
    }else{
      println("Elemento não existe na lista")
    }
    
    return novaLista
    
    return novaLista
  }
  
  override def removerElementoRecursiva(lista: No, valor: Integer): No ={
    var aux:No = lista
    if(aux.isEmpty(aux) == 0){
      if(aux.valorNo == valor){
        aux = aux.proximoNo
      }else if(aux.proximoNo.proximoNo != aux.proximoNo){
        aux.proximoNo = removerElementoRecursiva(aux.proximoNo, valor)
      }
    }
    return aux
  }
  override def liberarLista(lista: No): No = {
    var no:No = new No(null,null,null)
    return no
  }
}