object Main {
  def main(args: Array[String]): Unit = {
    //Primeira
    var lista = new No(null,null,null).criarLista()
    
    //Segunda
    lista = lista.inserirLista(lista, 1)
    lista = lista.inserirLista(lista, 42)
    lista = lista.inserirLista(lista, 3)
    lista = lista.inserirLista(lista, 6)
    lista = lista.inserirLista(lista, 47)
    lista = lista.inserirLista(lista, 7)
    lista = lista.inserirLista(lista, 22)
    
    //Terceira
    println("******************imprimir a lista******************")
    lista.imprimirLista(lista)
    
    //Quarta
    println("******************imprimir a lista recursiva******************")
    //lista.imprimirListaRecursiva(lista)
    
    //Quinta
    println("******************Lista vazia******************")
    println("Vazia? " + lista.isEmpty(lista))
    
    //Sexta
    println("******************Buscar elemento '1' e '6' vazia******************")
    println("Elemento 1: " + lista.buscarElemento(lista, 6).valorNo)
    println("Elemento 2: " + lista.buscarElemento(lista, 1).valorNo)
    
    //Sétima
    println("******************remmover elementos '1' e '6' da lista******************")
    lista = lista.removerElemento(lista, 1)
    lista = lista.removerElemento(lista, 6)
    lista.imprimirLista(lista)

    //Oitava
    println("******************remmover elemento '47' da lista recursivo******************")
    lista = lista.removerElementoRecursiva(lista, 47)
    lista.imprimirLista(lista)
   
    //Nona
    println("******************liberar a lista******************")
    lista = lista.liberarLista(lista)
    println(lista.isEmpty(lista))    
  }
}