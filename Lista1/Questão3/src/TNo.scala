trait TNo {
  def criarLista(): No
  def inserirLista(listaNo: No, valor: Integer): No
  def imprimirLista(lista: No): Unit
  def imprimirListaRecursiva(lista: No): Unit
  def imprimirListaReversa(lista: No): Unit
  def isEmpty(lista: No): Int
  def buscarElemento(lista: No, valor: Integer): No
  def removerElemento(lista: No, valor: Integer): No
  def removerElementoRecursiva(lista: No, valor: Integer): No
  def liberarLista(lista: No): No
  def isEquals(lista1:No, lista2:No): Boolean
}