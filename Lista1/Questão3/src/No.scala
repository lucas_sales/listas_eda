class No(ant: No,  prox: No, valor: Integer) extends TNo{
  
  var anteriorNo : No = ant
  var proximoNo : No = prox
  var valorNo: Integer = valor
  
  override def criarLista(): No = {
    var lista = new No(null,null,null)
    lista
  }
  override def inserirLista(listaNo: No, valor: Integer): No = {
    var novaLista = listaNo
    var aux = listaNo
    var novoElemento:No = new No(null,null,valor) 
    var ant:No = null
    var prox = listaNo.proximoNo
    
    if(novaLista.valorNo != null){
      while(prox != null && prox.valorNo < valor){
        ant = prox
        prox = prox.proximoNo
      }
      
      if(ant == null){
        novoElemento.proximoNo = aux.proximoNo
        if(aux.proximoNo != null){
          aux.proximoNo.anteriorNo = novoElemento
        }
        aux.proximoNo = novoElemento
        return aux
      }else{
        novoElemento.proximoNo = ant.proximoNo
        novoElemento.anteriorNo = ant
        if(ant.proximoNo != null){
          ant.proximoNo.anteriorNo = novoElemento
        }
        ant.proximoNo = novoElemento
        return novaLista
      }
      return null
    }else{
      novaLista.valorNo = valor
      return novaLista
    }
  }
  override def imprimirLista(lista: No): Unit ={
   var listaProx = lista

   if(lista.proximoNo != null){
     while(listaProx != null){
       println("Valor No:" + listaProx.valorNo)
       listaProx = listaProx.proximoNo
     }
   }else{
     println("Lista está vazia")
   }
   
  }
  
  override def imprimirListaRecursiva(lista: No): Unit = {
    if(lista != null){
      println("valor é: " + lista.valorNo)
      imprimirListaRecursiva(lista.proximoNo)
    }
  }

  override def imprimirListaReversa(lista: No): Unit = {
		if(lista != null){
      imprimirListaReversa(lista.proximoNo)
      println("valor é: " + lista.valorNo)
    }
  }
  override def isEmpty(lista: No): Int = {
    if(lista.valorNo == null)
      return 1
    else
      return 0
  }
  override def buscarElemento(lista: No, valor: Integer): No = {
    var noAux = lista
    while(noAux != null){
      if(noAux.valorNo == valor)
        return noAux
      noAux = noAux.proximoNo
    }
    return null
  }
  override def removerElemento(lista: No, valor: Integer): No ={
    var novaLista  = lista
    var aux = lista
    var ant:No = null 
    var elem = buscarElemento(lista, valor);
    if(elem != null){
      
      while(aux != null && aux.valorNo < valor){
        ant = aux
        aux = aux.proximoNo
      }
      
      if(ant == null){
        novaLista = novaLista.proximoNo
      }else{
        ant.anteriorNo = aux.proximoNo
        aux.proximoNo.anteriorNo = ant
        aux = ant
      }
      
    }else{
      println("Elemento não existe na lista")
    }
    
    return novaLista
  }
  
  override def removerElementoRecursiva(lista: No, valor: Integer): No ={
    var aux:No = lista
    if(aux.isEmpty(aux) == 0){
      if(aux.valorNo == valor){
        if(aux.proximoNo != null)
          aux.proximoNo.anteriorNo = aux.anteriorNo
        aux = aux.proximoNo
      }else{
        aux.proximoNo = removerElementoRecursiva(aux.proximoNo, valor)
      }
    }
    return aux
  }
  override def liberarLista(lista: No): No = {
    var no:No = new No(null,null,null)
    return no
  }
  
  override def isEquals(lista1:No, lista2:No): Boolean = {
    var lst1 = lista1
    var lst2 = lista2
    while(lst1 != null && lst2 != null){
      if(lst1.valorNo != lst2.valorNo)
        return false
      
      lst1 = lst1.proximoNo
      lst2 = lst2.proximoNo
    }
    
    if(lst1 != null || lst2 != null)
      return false
    else 
      return true
  }
}