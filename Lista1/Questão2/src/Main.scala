object Main {
  def main(args: Array[String]): Unit = {
    //Primeira
    var lista = new No(null,null).criarLista()
    
    //Segunda
    lista = lista.inserirLista(lista, 1)
    lista = lista.inserirLista(lista, 42)
    lista = lista.inserirLista(lista, 3)
    lista = lista.inserirLista(lista, 6)
    lista = lista.inserirLista(lista, 47)
    lista = lista.inserirLista(lista, 7)
    lista = lista.inserirLista(lista, 22)
    
    //Terceira
    println("******************imprimir a lista******************")
    lista.imprimirLista(lista)
    
    //Quarta
    println("******************imprimir a lista recursiva******************")
    lista.imprimirListaRecursiva(lista)
    
    //Quinta
    println("******************imprimir a lista reversa******************")
    lista.imprimirListaReversa(lista)
    
    //Sexta
    println("******************Lista vazia******************")
    println("Vazia? " + lista.isEmpty(lista))
    
    //Sétima
    println("******************Buscar elemento vazia******************")
    println("Elemento 1: " + lista.buscarElemento(lista, 6).valorNo)
    println("Elemento 2: " + lista.buscarElemento(lista, 1).valorNo)
    
    //Oitava
    println("******************remmover elementos '1' e '6' da lista******************")
    lista = lista.removerElemento(lista, 1)
    lista = lista.removerElemento(lista, 6)
    lista.imprimirLista(lista)
    
    
    //Nona
    println("******************remmover elementos lista recursivo******************")
    lista = lista.removerElementoRecursiva(lista, 42)
    lista.imprimirLista(lista)
   
    //Décima
    println("******************Liberar lista******************")
    lista = lista.liberarLista(lista)
    lista.imprimirLista(lista)
    
    //Décima primeira
    println("******************Liberar lista******************")
    var lista2 = new No(null,null).criarLista()
    var lista3 = new No(null,null).criarLista()

    lista3 = lista3.inserirLista(lista3, 1)
    lista3 = lista3.inserirLista(lista3, 42)
    lista3 = lista3.inserirLista(lista3, 3)
    
    lista2 = lista2.inserirLista(lista2, 1)
    lista2 = lista2.inserirLista(lista2, 42)
    lista2 = lista2.inserirLista(lista2, 3)
    
    if(lista.isEquals(lista2, lista3))
      println("Listas são iguais")
    else
      println("Listas são diferentes")
  }  
}