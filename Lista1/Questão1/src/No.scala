class No(prox: No, valor: Integer) extends TNo{
  
  var proximoNo : No = prox
  var valorNo: Integer = valor
  
  override def criarLista(): No = {
    var lista = new No(null,null)
    lista
  }
  override def inserirLista(lista: No, valor: Integer): No = {
    if(lista.valorNo != null){
      var novaLista = new No(null,valor)
      novaLista.proximoNo = lista
      
      return novaLista
    }else{
       var novaLista = new No(null,valor)
      return novaLista
    }
  }
  override def imprimirLista(lista: No): Unit ={
   var listaProx = lista

   if(lista.valorNo != null){
     while(listaProx != null){
       println("Valor No:" + listaProx.valorNo)
       listaProx = listaProx.proximoNo
     }
   }else{
     println("Lista está vazia")
   }
   
  }
  
  override def imprimirListaRecursiva(lista: No): Unit = {
    if(lista != null){
      println("valor é: " + lista.valorNo)
      imprimirListaRecursiva(lista.proximoNo)
    }
  }

  override def imprimirListaReversa(lista: No): Unit = {
    if(lista != null){
      imprimirListaReversa(lista.proximoNo)
  	  println("valor é: " + lista.valorNo)
    }
  }
  override def isEmpty(lista: No): Int = {
    if(lista.valorNo == null)
      return 1
    else
      return 0
  }
  override def buscarElemento(lista: No, valor: Integer): No = {
    var noAux = lista
    while(noAux != null){
      if(noAux.valorNo == valor)
        return noAux
      noAux = noAux.proximoNo
    }
    return null
  }
  override def removerElemento(lista: No, valor: Integer): No ={
    var novaLista  = lista
    var aux = lista
    var ant:No = null 
    if(buscarElemento(lista, valor) != null){
      
      while(aux != null && aux.valorNo != valor){
        ant = aux
        aux = aux.proximoNo
      }
      
      if(ant == null){
        novaLista = novaLista.proximoNo
      }else{
        ant.proximoNo = aux.proximoNo
        aux = ant
      }
      
    }else{
      println("Elemento não existe na lista")
    }
    
    return novaLista
  }
  
  override def removerElementoRecursiva(lista: No, valor: Integer): No ={
    var aux:No = lista
    if(aux.isEmpty(lista) == 0){
      if(aux.valorNo == valor){
        aux = aux.proximoNo
      }else{
        aux.proximoNo = removerElementoRecursiva(aux.proximoNo, valor)
      }
    }
    return aux
  }
  override def liberarLista(lista: No): No = {
    var no:No = new No(null,null)
    return no
  }
}