package ufc.br.eda

object Main {
  def main(args: Array[String]): Unit = {
    var impl : Impl = new Impl
     
     var pa : Particao = impl.makeSet(2)
     
     var pa2 : Particao = impl.makeSet(5)
     
     var parUniao : Particao = impl.union(pa, pa2) 
     
     var pa3 : Particao = impl.makeSet(9)
     
     impl.print(impl.union(parUniao, pa3))
     
     var no1: No = parUniao.inicio
     var no2: No = parUniao.inicio.proximo
     
     if(impl.find(no1).equals(impl.find(no2))){
       println("Os nós contém os mesmos representantes")
     }else{
       println("Não contém os mesmos representantes")
     }
     
     var estruturaLiberada : Particao = impl.liberarEstrutura(parUniao)
     
     println(estruturaLiberada)
  }
}