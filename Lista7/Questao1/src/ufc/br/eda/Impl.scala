package ufc.br.eda

class Impl {
  def makeSet(valor : Integer) : Particao = {
      
    var particao : Particao = new Particao()
    var no : No = new No()
    no.valor = valor
    no.representante = no
    no.proximo = null
    
    particao.inicio = no
    particao.fim = no
    particao.tamanho = 1
    
    particao
   }
  
  def find(no : No) : No = no.representante
  
  def union(par1 : Particao, par2 : Particao) : Particao = {
    if(par1==null || par2==null){
      println("Elementos não estão iniciados")
      return null
    }
    
    var maior : Particao = par2
    var menor : Particao = par1
    
    if(par1.tamanho > par2.tamanho){
      maior = par1
      menor = par2
    }
      
    var fim : No = maior.fim
    
    maior.fim = menor.fim
    fim.proximo = menor.inicio
    
    var inicioMenor : No = menor.inicio
    
    while(inicioMenor!=null){
      inicioMenor.representante = maior.inicio
      inicioMenor = inicioMenor.proximo
    }
    
    maior.tamanho+=menor.tamanho
    
    maior
  }
  
  
  
  def liberarEstrutura(particao : Particao) : Particao = {
    particao.inicio = null
    particao.fim = null
    particao.tamanho = 0
    null
  }
  
  def print(particao : Particao){
    var no : No = particao.inicio
    while(no!=null){
      println(no.valor)
      no = no.proximo
    }
   }
  
}