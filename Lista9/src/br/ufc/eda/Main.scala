package br.ufc.eda

object Main {
  def main(args: Array[String]) {
    var abb: Tree = new Tree
    abb.insert(5)
    abb.insert(4)
    abb.insert(6)
    abb.insert(7)
    abb.insert(3)
    abb.insert(2)
    abb.insert(10)
    println("Valor encontrado: " + abb.find(3))

    abb.imprimir()
    abb.remove(3)
    abb.imprimir()
    abb.find(3)

    println("Maior Valor: " + abb.findMax().value)
    println("O pai é: " + abb.findParent(4).value)
    
    abb.change(5)
    abb.imprimir()
    

  }
}