package br.ufc.eda

class Node {
  var value: Integer = 0
  var parent: Node = null
  var nodeR: Node = null
  var nodeL: Node = null
}