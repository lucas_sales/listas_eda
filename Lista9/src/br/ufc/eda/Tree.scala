package br.ufc.eda

class Tree {
  var root: Node = new Node

  def isEmpty(): Boolean = {
    if (root == null) {
      return true;
    }
    return false;
  }

  def getAltura(): Int = {
    return getAltura(this.root)
  }

  private def getAltura(root: Node): Int = {
    if (root == null) {
      return 0;
    }
    var altL = getAltura(root.nodeL)
    var altR = getAltura(root.nodeR)
    if (altL > altR) {
      return altL + 1
    } else {
      return altR + 1
    }
  }

  def getQtdNode(): Int = {
    return getQtdNode(root)
  }

  private def getQtdNode(root: Node): Int = {
    if (root == null) {
      return 0;
    }
    var qtdNodeL = getQtdNode(root.nodeL)
    var qtdNodeR = getQtdNode(root.nodeR)
    return qtdNodeL + qtdNodeR + 1
  }

  def imprimir() {
    if (root == null)
      println("Árvore vazia");
    else
      imprimir(root);
  }

  private def imprimir(node: Node) {
    if (node.nodeL != null) {
      imprimir(node.nodeL)
    }
    if (node.nodeR != null) {
      imprimir(node.nodeR)
    }
    println("Nó: " + node.value)
  }

  def insert(value: Int) {
    insert(this.root, value)
  }

  private def insert(node: Node, value: Int): Unit = {
    var newnode: Node = new Node
    if (this.root.value == 0) {
      newnode.value = value
      root = newnode
    } else {
      if (value < node.value) {
        if (node.nodeL != null) {
          insert(node.nodeL, value)
        } else {
          newnode.value = value
          node.nodeL = newnode
        }
      } else if (value > node.value) {
        if (node.nodeR != null) {
          insert(node.nodeR, value)
        } else {
          newnode.value = value
          node.nodeR = newnode

        }
      }
    }
  }

  private def insertTreeN(node: Node, values:Array[Integer]): Unit = {
    var newnode: Node = new Node
    var i = 0
    while(i < values.length){
      insert(values(i))
      i += 1
    }
  }

  def remove(value: Int): Node = {
    return remove(root, value)
  }

  private def remove(node: Node, value: Int): Node = {
    if (this.root == null) {
      println("Árvore vazia")
      return node
    } else {
      if (value < node.value) {
        node.nodeL_=(remove(node.nodeL, value))
      } else if (value > node.value) {
        node.nodeR_=(remove(node.nodeR, value))
      } else if (node.nodeL != null && node.nodeR != null) {
        println("  Removeu No " + node.value)
        node.value = encontraMinimo(node.nodeR).value
        node.nodeR = removeMinimo(node.nodeR)
      } else {
        println("  Removeu No " + node.value);
        if (node.nodeL != null) {
          var changeNode = node.nodeL

          return changeNode

        } else {
          var changeNode = node.nodeR

          return changeNode
        }
      }
      return node;
    }
  }

  def removeMinimo(node: Node): Node = {
    if (node == null) {
      println("  ERRO ")
    } else if (node.nodeL != null) {
      node.nodeL = (removeMinimo(node.nodeL));
      return node;
    } else {
      return node.nodeR;
    }
    return null;
  }

  private def encontraMinimo(node: Node): Node = {
    var newNode = new Node
    newNode = node
    if (node != null) {
      while (newNode.nodeL != null) {
        newNode = newNode.nodeL
      }
    }
    return newNode
  }

  def findMax(): Node = {
    return findMax(root)
  }

  private def findMax(node: Node): Node = {
    var newNode = new Node
    newNode = node
    if (node != null) {
      while (newNode.nodeR != null) {
        newNode = newNode.nodeR
      }
    }
    return newNode
  }

  def find(value: Int): Node = {
    return find(root, value)
  }

  private def find(node: Node, value: Int): Node = {
    if (node == null) {
      println("Valor não encontrado")
      return null
    } else {
      if (value == node.value) {
        return node
      } else if (value < node.value) {
        find(node.nodeL, value)
      } else {
        find(node.nodeR, value)
      }

    }
  }

  def findParent(value: Int): Node = {
    return findParent(root, null, value)
  }

  private def findParent(nodeActual: Node, nodeParent: Node, value: Int): Node = {
    if (nodeActual == null) {
      println("Valor não encontrado")
      return null
    } else {
      if (value == nodeActual.value) {
        return nodeParent
      } else if (value < nodeActual.value) {
        var parent = nodeActual
        findParent(nodeActual.nodeL, parent, value)
      } else {
        var parent = nodeActual
        findParent(nodeActual.nodeR, parent, value)
      }

    }
  }
  
  def change(value:Integer){
    var node:Node = find(value)
    if(node != null)
      return change(node)
  }
  
  private def change(node:Node){
    if(node.nodeL != null)
      change(node.nodeL)
    if(node.nodeR != null)
      change(node.nodeR)
    if(node.nodeL != null && node.nodeR != null){
      val aux = node.nodeL.value
      node.nodeL.value = node.nodeR.value
      node.nodeR.value = aux
    }
  }
  
  

}