package ufc.br.great

import scala.collection.mutable.ListBuffer

class HashTable {
  var table: ListBuffer[Element] = null
  var bitRep: Int = 0
  var tamanhoBucket: Int = 0
  
  def funcaoHash(valor:Int): String ={
    valor.toBinaryString
  }
  
  def getBucket(valor:Int): Int ={
    
    
    if(bitRep > 0){
      var key = funcaoHash(valor)
      var endString: String = key.takeRight(bitRep)
      val index = endString.toInt
      return index
    }else{
      return -1
    }
    
//    var i = 0
//    
//    while(i <= index){
//      table.lift(i) match {
//        case Some(x) => println(x)
//        case None =>{ 
//          var b:Bucket = new Bucket();
//          b.inicializarLista()
//          table += b
//        }
//      }
//      i += 1
//    }
//
//    
//    return index
  }
  
  def criarHash(tamanhoBucket:Int): Unit ={
    this.tamanhoBucket = tamanhoBucket
    table = new ListBuffer[Element]()
    var b:Bucket = new Bucket();
    b.bitRep = 1
    b.inicializarLista()
    var element2:Element = new Element();
    var element1:Element = new Element();
    element1.bucket = b
    element2.bucket = b
    table += element1
    table += element2
  }
  
  def inserir(valor:Int): Unit ={
    var indexBucket = getBucket(valor)
    
    if(indexBucket < 0){
      if(valor%2 == 0){
        if(bitRep > table(0).bucket.bitRep){
          
        }else{
          if(table(0).bucket.elementos.size < tamanhoBucket){
            table(0).bucket.elementos += valor
            println("Inseriu: " + valor)
          }else{
            //dividindo o bucket
            bitRep += 1
            var bPares:Bucket = new Bucket()
            bPares.bitRep = 1
            bPares.inicializarLista()
            bPares.elementos = table(0).bucket.elementos.filter(e => e%2 == 0)
            table(0).bucket = bPares
            table(0).bucket.elementos += valor
            table(1).bucket.elementos = table(1).bucket.elementos.filter(e => e%2 != 0)
            println("Inseriu: " + valor)
          }
        }
      }else{
        if(bitRep > table(1).bucket.bitRep){
          
        }else{
          if(table(1).bucket.elementos.size < tamanhoBucket){
            table(1).bucket.elementos += valor
            println("Inseriu: " + valor)
          }else{
            //tem q dividir o bucket
            bitRep += 1
            var bPares:Bucket = new Bucket()
            bPares.bitRep = 1
            bPares.inicializarLista()
            bPares.elementos = table(0).bucket.elementos.filter(e => e%2 == 0)
            table(0).bucket = bPares
            table(1).bucket.elementos = table(1).bucket.elementos.filter(e => e%2 != 0)
            table(1).bucket.elementos += valor
            println("Inseriu: " + valor)
          }
        }
      }
      
    }else{ 
     
      if(bitRep > table(indexBucket).bucket.bitRep){
        //fazer com que os elementos do buckt fiquem no local certo e add o novo elemento        
        //lembrando que tem que  pegar 1bit de representação a menos
        //***
        var key = funcaoHash(valor)
        var endString: String = key.takeRight(bitRep)
        val index = endString.toInt
        //***
        var b:Bucket = new Bucket();
        b.bitRep = table(indexBucket).bucket.bitRep + 1
        b.inicializarLista()
        
        b.elementos = table(indexBucket).bucket.elementos.filter(e =>{
          var k = funcaoHash(e)
          var f: String = k.takeRight(indexBucket)
          val i = endString.toInt
          i == index
        })
        table(indexBucket).bucket  = b
        
      }else{
      //BitRep é igual ao bitRep do bitbuckt
       
      
      //verifica se pode inserir elemento bucket
        if(table(indexBucket).bucket.elementos.size < tamanhoBucket){
          table(indexBucket).bucket.elementos += valor
          println("Inseriu: " + valor)
        }else{
          //aumentar o bit
          var i = Math.pow(2,bitRep)
          i = i - 1;
          var stop = Math.pow(2, bitRep+1)
          //faz com que a hash aponte para os buckets
          while(i < stop){
            var e:Element = new Element()
            i += 1
            table += e
            table(i.intValue()).bucket = table(indexBucket).bucket
          }
          
          bitRep += 1
          inserir(valor)
        }
      }
    }
  
  
  }
  
}