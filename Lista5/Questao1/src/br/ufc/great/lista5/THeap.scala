package br.ufc.great.lista5

trait THeap {
  def criar(tamanho:Int): Unit
  def inserir(valor:Int): Unit
  def remover(): Unit
  def buscar(valor:Int): Int
  def alterValor(valor:Int, novoValor:Int): Unit
  def liberar(): Unit
}