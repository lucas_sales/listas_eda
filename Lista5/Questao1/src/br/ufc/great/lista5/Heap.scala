package br.ufc.great.lista5


class Heap extends THeap {
  var heap:Array[Int] = null
  var posLivre = -1

  override def criar(tamanho:Int): Unit ={
    heap = new Array[Int](tamanho)
    this.posLivre = 0
  }
  
  override def inserir(valor:Int): Unit ={
    if(this.posLivre < heap.size){
      heap(posLivre) = valor
      corrigeAcima(posLivre)
      posLivre += 1
    }else{
      println("Heap esta cheia")
    }
  }
  
  def corrigeAcima(pos:Int): Unit ={
    var i = pos
    var p = pos
    while(i > 0){
      var pai = (p-1)/2
      if(heap(pai)  <  heap(p)){
        var aux = heap(p)
        heap(p) = heap(pai)
        heap(pai) = aux
      }else{
        i = - 1;
      }
      p = pai
      i = p
    }
  }
  
  override def remover(): Unit ={
    if(posLivre > 0){
      var topo = heap(0)
      heap(0) = heap(posLivre-1)
      posLivre = posLivre - 1
      corrigeAbaixo()
      println("O elemento: " + topo + " foi removido")
    }else{
      println("Heap Vazia")
    }
  }
  
  def corrigeAbaixo(): Unit ={
    var pai = 0
    var i = 0
    while(2*i+1 < posLivre){
      var filhoEsq = 2*pai+1
      var filhoDir = 2*pai+2
      var filho = -1
      
      if(filhoDir > posLivre)
        filhoDir = filhoEsq
      if(heap(filhoEsq) > heap(filhoDir))
        filho = filhoEsq
      else
        filho = filhoDir
      if(heap(pai) < heap(filho)){
        var aux = heap(pai)
        heap(pai) = heap(filho)
        heap(filho) = aux
      }else{
        i = -2
      }
      
      pai = filho
      i = pai
    }
  
  }
  
  override  def buscar(valor:Int): Int = {
    heap.find(x => x == valor) match{
      case Some(x) => return x
      case None => return -1
    }
  }
  
  override def alterValor(valor:Int, novoValor:Int): Unit = {
    if(buscar(valor) != -1){
      heap.indexWhere(v => v == valor) match{
        case i =>{
          heap(i) = novoValor
          var posPai:Int = (i-1)/2
          if(heap(i) > heap(posPai)){
          	corrigeAcima(i)
          }else if(heap(i) < heap(posPai)){
          	corrigeAbaixo()
          }
        }
      }
    }
  }
  
  override def liberar(): Unit = {
    heap = null
    posLivre = -1
  }
  
  
  
}