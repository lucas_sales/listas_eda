package br.ufc.great.lista5

object Main {
  def main(args: Array[String]): Unit = {
    var h:Heap = new Heap()
    
    h.criar(10)
    
    h.inserir(4)
    h.inserir(43)
    h.inserir(55)
    h.inserir(2)
    h.inserir(1)
    
    
    val valor = h.buscar(2)
    println("valor eh: " + valor)
    
    h.alterValor(1, 8)
    
    h.remover()
    h.remover()
    h.remover()
    
    h.liberar()
    
  }
}