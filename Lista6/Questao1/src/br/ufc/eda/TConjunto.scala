package br.ufc.eda

trait TConjunto {
  def criaConjunto(n : Int, valores : Array[String]): Unit
  def insereConjunto(valor : String): Unit
  def removeConjunto(valor : String): Unit
  def uniaoConjuntos(conj : Conjunto): Int
  def intersecaoConjuntos(conj : Conjunto): Int
  def diferencaConjuntos(conj : Conjunto): Int
  def subConjunto(conj : Conjunto): Boolean
  def conjuntosIguais(conj : Conjunto): Boolean
  def complemento(): Int
  def pertence(valor : String): Boolean
  def getQtdElementosConjunto(): Int
  def free(): Unit
}