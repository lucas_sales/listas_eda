package br.ufc.eda

object Main {
  def main(args: Array[String]): Unit = {
    var valores : Array[String]  = new Array[String](5)	
		valores(0) += "a"
		valores(1) += "b"
		valores(2) += "c"
		valores(3) += "d"
		valores(4) += "e"
		
		var conjunto : Conjunto = new Conjunto()	
		conjunto.criaConjunto(5, valores)
		
		conjunto.insereConjunto("a")
		conjunto.insereConjunto("b")
		conjunto.insereConjunto("c")
		conjunto.imprimirConjunto()
		
		conjunto.removeConjunto("c")
		conjunto.imprimirConjunto()
		
		var conjunto2 : Conjunto = new Conjunto()	
		conjunto2.criaConjunto(5, valores)
		conjunto2.insereConjunto("a")
		conjunto2.insereConjunto("b")
		
		conjunto2.bits = (conjunto.uniaoConjuntos(conjunto2))
		
		conjunto2.imprimirConjunto()
		
		var conjunto3 : Conjunto = new Conjunto()	
		conjunto3.criaConjunto(5, valores)
		conjunto3.insereConjunto("b")
		conjunto3.insereConjunto("a")
		
		conjunto3.bits = (conjunto.intersecaoConjuntos(conjunto3))
		conjunto3.imprimirConjunto()
		
		conjunto2.bits = (conjunto2.diferencaConjuntos(conjunto3))
		conjunto2.imprimirConjunto()
		
		conjunto.imprimirConjunto()
		
		if(conjunto.subConjunto(conjunto2))
			println("Conjunto 2 é subconjunto de conjunto 1")
		else
			println("Conjunto 2 não é subconjunto de conjunto 1")
		
		if (conjunto.conjuntosIguais(conjunto2))
			println("Conjuntos iguais\n")
		else
			println("Conjuntos diferentes\n")
		
		conjunto3.bits = (conjunto.complemento())
		conjunto3.imprimirConjunto()
		
		if(conjunto.pertence("a"))
			println("Pertence \n")
		else
			println("Não Pertence \n")
		
		println(conjunto.getQtdElementosConjunto())
		
		conjunto.free()
		conjunto2.free()
  }
}