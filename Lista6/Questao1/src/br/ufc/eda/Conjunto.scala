package br.ufc.eda

class Conjunto extends TConjunto{
  var conjunto : Array[String] = null
  var bits: Int = 0
  
	override def criaConjunto(n : Int, valores : Array[String]): Unit ={
  		this.conjunto = valores
  		bits = 0
	}
  
	override def insereConjunto(valor : String): Unit ={
		for(i <- 0 to conjunto.length-1){
			if(conjunto(i).equals(valor)){
				ligarBit(i)
				return
			}
		}
	}
  
	override def removeConjunto(valor : String): Unit ={
		for(i <- 0 to conjunto.length-1){
			if(conjunto(i).equals(valor)){
				desligarBit(i)
				return
			}
		}
	}
  
	override def uniaoConjuntos(conj : Conjunto): Int ={
		conj.bits | bits
	}
  
	override def intersecaoConjuntos(conj : Conjunto): Int ={
		conj.bits & bits
	}
  
  override def diferencaConjuntos(conj : Conjunto): Int ={
		if(conj.bits < bits){
			bits - conj.bits
		}else{
			conj.bits - bits
		}
	}
  
	override def subConjunto(conj : Conjunto): Boolean ={
		if((bits & conj.bits) == conj.bits && (bits | conj.bits) == bits){
			true
	  }
		false
	}
  
	override def conjuntosIguais(conj : Conjunto): Boolean = (bits == conj.bits)
	

	override def complemento(): Int = ~bits
	
  
	override def pertence(valor : String): Boolean ={
		for(i <- 0 to conjunto.length-1)
			if(conjunto(i).equals(valor))
				estaLigado(i)
		
		false
	}
  
	override def getQtdElementosConjunto(): Int ={
		var valor : Int = bits
		var bit : Int = 0
		var contador : Int = 0
		
		while(valor != 0){
			bit = valor%2
			if(bit == 1)
				contador += 1
			valor = valor/2
		}
		
		contador
	}
  
	override def free(): Unit = conjunto = null
	
	
	def ligarBit(i : Int): Unit ={
		if(!estaLigado(i))
			bits += scala.math.pow(2, i).toInt
	}
	
	def desligarBit(i : Int): Unit ={
		if(estaLigado(i))
			bits -= scala.math.pow(2, i).toInt
	}
  	
	def estaLigado(i : Int) : Boolean = {
		var mask : Int =  1 << i
		((mask & bits) != 0)
	}
	
	def imprimirConjunto(): Unit ={
		for(i <- 0 to conjunto.length-1){
			if(estaLigado(i))
				println(conjunto(i))
		}
		println()
	}
}