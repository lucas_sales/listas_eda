package br.ufc.mestrado.lista3.q1

trait THashTable {
  def create(tamanhoHash:Int)
  def inserir(valor:Int)
  def buscar(valor:Int)
  def remover(valor:Int)
  def free()
}